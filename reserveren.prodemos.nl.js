// var scriptinject = document.createElement('script');
// scriptinject.setAttribute('src','//bb.githack.com/Timmeer/reserveren-extentions/raw/master/reserveren.prodemos.nl.js');
// document.head.appendChild(scriptinject);
// //
(function() {
"use strict"
//load various jquery plugins

//autocomplete picker van jquery.
addjavascript('//cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.jquery.min.js');
addjavascript('//cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js');
addjavascript('//cdn.rawgit.com/christianbach/tablesorter/master/jquery.tablesorter.min.js');

var jquerychosencss = document.createElement('link');
jquerychosencss.setAttribute("rel", "stylesheet");
jquerychosencss.setAttribute("type", "text/css")
jquerychosencss.setAttribute('href','//cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.min.css');
document.head.appendChild(jquerychosencss);

// add javascript
function addjavascript(link){
	var scripttag = document.createElement('script');
	scripttag.setAttribute('src', link);
	document.head.appendChild(scripttag);
}



//username en initialen gebaseerd op 'U bent ingelogd als: Tim Meerhof'
var myName = $('.username').html();
console.log(myName);
var names = myName.split(' ');
var myInitials = "";
for (var i = 0; i < names.length; i++) {
  myInitials += names[i].substr(0,1);
};

var month = [ "jan","feb","mrt","apr","mei","jun","jul","aug","sep","okt","nov","dec"];

//use to know wich page where on.
var urlSigmenten = document.URL.split('/');

/***********************************************
*****AANVRAGEN LIJST***************************
***********************************************/
if(urlSigmenten[4] == "aanvragen" ) {
  var tabledivaanvraag = $(".aanvragen");
  if(tabledivaanvraag.length > 0){
    //connect the table sorter.
    tabledivaanvraag.addClass('tablesorter');
    $( document ).ready(function() {
        setTableSorter('.aanvragen');
    });
    var docentenarray = [];
    var programmaArray = [];
    var columnumberaanvrager = 6;
    var columnumberprogramma = 1;
    var columnumberDate = 0;
    var columnumberAanvraag = 7;

    var aanvraagbody = tabledivaanvraag[0].children[1];
    var rows = aanvraagbody.getElementsByTagName('tr');
    //console.log(rows);
    for(var index in rows){
      if(rows[index].children){
        docentenarray.push(rows[index].children[columnumberaanvrager].innerHTML);
        programmaArray.push(rows[index].children[columnumberprogramma].innerHTML);
        var columnumberschool = 4;
        if(rows[index].children[columnumberschool].childNodes.length > 1){
          var adres = encodeURIComponent(rows[index].children[columnumberschool].children[1].innerHTML);

          var link = $('<a>Maps</a>');
          var vakje = $(rows[index].children[columnumberschool]);
          vakje.append(link);
          vakje.attr('id','maps_'+index );
          link.click( function(e){
            var adres = "";
            $(this).parent().children().each(function(i, itm) {
              var item = $(itm).html();
              if(item != 'Maps'){
                adres += "%20" + encodeURIComponent(item.trim());
              }                         
            });

            var cell = $('<td></td>');
            var linkmaps = $('<a href="https://www.google.nl/maps/search/'+adres+'" target="_blank"><img  src="https://maps.googleapis.com/maps/api/staticmap?center=Utrecht&zoom=6&size=200x200&markers=color:red|label:S|'+adres+'"></img></a>')
            var linkclose = $('<a>X</a>');
            cell.append(linkclose);
            cell.append(linkmaps);
            linkclose.click(function (e) {
              $(this).parent().remove();
            });
            $(this).parent().append(cell);

          });

        }

        var datumVak = $(rows[index].children[columnumberDate]);
        var datumString = getDate(datumVak.html());
        datumVak.html(datumString);

        var AanvraagVak = $(rows[index].children[columnumberAanvraag]);
        var datumString = getDate(AanvraagVak.children('div:last').html(), true);
        AanvraagVak.html(datumString);
      }
    }
    var docentencount = countVal(docentenarray);
    var programmacount = countVal(programmaArray);

    //docent
    var _select = $('<select>').attr('id', 'gastdocent_select');
    
    addautocomplete('#gastdocent_select');
    
    _select.append(
      $('<option></option>').val(-1).html('kies docent')
      );
    $.each(docentencount[0], function(index5, name) {
      _select.append(
        $('<option></option>').val(index5).html(name)
        );
    });

    //PROGRAMMA
    var _selectProg = $('<select>');

    _selectProg.append(
      $('<option></option>').val(-1).html('kies programma')
      );
    $.each(programmacount[0], function(index6, name) {
      _selectProg.append(
        $('<option></option>').val(index6).html(name)
        );
    });

    //docent
    _select.change(function() {
      var docent = _select.children("option").filter(":selected").text();
      var index = _select.val();
      for(var index in rows){
        if(rows[index].children && rows.length >  index){
          var naam = rows[index].children[columnumberaanvrager].children[0].innerHTML; 
          console.log(naam + ' '+ docent);
          if(naam != docent && docent != 'kies docent'){
            $(rows[index]).hide();
          } else {
            $(rows[index]).show();
          }

        } 
      }

    });

    //PROGRAMMA
    _selectProg.change(function() {
      var docent = _selectProg.children("option").filter(":selected").text();
      var index = _selectProg.val();
      for(var index in rows){
        if(rows[index].children && rows.length >  index){
          var naam = rows[index].children[columnumberprogramma].innerHTML; 
          console.log(naam + ' '+ docent);
          if(naam != docent && docent != 'kies programma'){
            $(rows[index]).hide();
          } else {
            $(rows[index]).show();
          }

        } 
      }

    });
  $('#middleandright_column').prepend(_selectProg);
  $('#middleandright_column').prepend(_select);

    for(var index in rows){

     if(rows[index].children && rows.length >  index){
       var naam = rows[index].children[columnumberaanvrager].innerHTML
       for(var index2 in docentencount[0]){

         if(naam == docentencount[0][index2]){
           var aanvraagDiv = $(rows[index].children[columnumberaanvrager]);

           aanvraagDiv.html($('<span>').html(aanvraagDiv.html()));
           aanvraagDiv.append(" \n[" + docentencount[1][index2] + "]");
           aanvraagDiv.append(getSelectLink(index2));
         }
       }
     }
   }
  


 }
}



/***********************************************
*****GASTLESSEN OVERZICHT***********************
***********************************************/
if(urlSigmenten[7] == 'GL') {
  addautocomplete('#shop_option_plgn_filters_begeleider_id');
  $('#shop_option_plgn_filters_begeleider_id').width('250px');
    addautocomplete('#shop_option_plgn_filters_partner_id');
  $('#shop_option_plgn_filters_partner_id').width('250px');

  var tabledivlijst = $(".gray");
  var tabledivlijst;
  //debugger;
  if(tabledivlijst.length > 0){
    var docentenarray = [];
    //begeleidersarray = [];
    var begeleiders = $('.ar_status_change16');
    var tabledivlijstbody = tabledivlijst[0].children[1];
    var rows = tabledivlijstbody.getElementsByTagName('tr');

	  //connect the table sorter.
	tabledivlijst.addClass('tablesorter');
	$( document ).ready(function() {
	  	setTableSorter('.gray'); 
	});

    var columnumberaanvrager = 8;
    var columnumberschool = 5;
    var colmnnevenementop = 6;
    var colmnnbegeleider = 4;
    for(var index in rows){
      if(rows[index].children && rows.length - 1 >  index){
       docentenarray.push(rows[index].children[columnumberaanvrager].children[0].innerHTML)
       var email = rows[index].children[columnumberaanvrager].children[2].innerHTML;
       var gastles = rows[index].children[2].children[0].innerHTML;
       var naam = rows[index].children[columnumberaanvrager].children[0].innerHTML;
       if(rows[index].children[columnumberschool].childNodes.length > 1){
        gastles += ' ' + rows[index].children[columnumberschool].children[0].innerHTML;

        var adres = encodeURIComponent(rows[index].children[columnumberschool].children[1].innerHTML);

        var link = $('<a>Maps</a>');
        var vakje = $(rows[index].children[columnumberschool]);
        vakje.append(link);
        vakje.attr('id','maps_'+index );
        link.click( function(e){
          var adres = encodeURIComponent($(this).parent().children('div:nth-child(2)').html());
          var cell = $('<td></td>');
          var linkmaps = $('<a href="https://www.google.nl/maps/search/'+adres+'" target="_blank"><img  src="https://maps.googleapis.com/maps/api/staticmap?center=Utrecht&zoom=6&size=200x200&markers=color:red|label:S|'+adres+'"></img></a>')
          var linkclose = $('<a>X</a>');
          cell.append(linkclose);
          cell.append(linkmaps);
          linkclose.click(function (e) {
            $(this).parent().remove();
          });
          $(this).parent().append(cell);

        });

      }
      var evenement = $(rows[index].children[colmnnevenementop]);

      if(evenement.children() && evenement.children().length > 0){

        var link = $('<a>Maps</a>');
        evenement.append(link);

        link.click( function(e){
          var adres = "";
          $(this).parent().children().each(function(i, itm) {
           var item = $(itm).html();
           if(item != 'Maps'){
            adres += "%20" + encodeURIComponent(item.trim());
          }                         
        });
          adres = adres.replace('(partner)', '');
          var cell = $('<td></td>');
          var linkmaps = $('<a href="https://www.google.nl/maps/search/'+adres+'" target="_blank"><img  src="https://maps.googleapis.com/maps/api/staticmap?center=Utrecht&zoom=6&size=200x200&markers=color:red|label:S|'+adres+'"></img></a>')
          var linkclose = $('<a>X</a>');
          cell.append(linkclose);
          cell.append(linkmaps);
          linkclose.click(function (e) {
            $(this).parent().remove();
          });
          $(this).parent().append(cell);

        });

        }

        rows[index].children[columnumberaanvrager].innerHTML += '<a docent="'+naam+'" id="link_'+index+'" href="mailto:'+email+'?Subject='+gastles+'" target="_top">email</a>';

        $("#link_" + index).click(function(event) {
          //copyToClipboard("Beste "+email+",\n\n");
          //console.log(event.target.attributes.docent.value);
          copyToClipboard("Beste "+event.target.attributes.docent.value+",\n\n")
        });
      }
    }

    var docentencount = countVal(docentenarray);

    var _select = $('<select>').attr('id', 'gastdocent_select');
     
     addautocomplete('#gastdocent_select');
     $('#gastdocent_select').width('250px');


    _select.append(
      $('<option></option>').val(-1).html('kies docent')
      );
    $.each(docentencount[0], function(index5, name) {
      _select.append(
        $('<option></option>').val(index5).html(name)
        );
    });

    _select.change(function() {
      var docent = _select.children("option").filter(":selected").text();
      var index = _select.val();
      for(var index in rows){
        if(rows[index].children && rows.length - 1 >  index){
         var naam = rows[index].children[columnumberaanvrager].children[0].innerHTML; 
        // console.log(naam + ' '+ docent);
         if(naam != docent && docent != 'kies docent'){
          $(rows[index]).hide();
        } else {
          $(rows[index]).show();
        }

      } 
    }

  });

    $('#shop_option_plgn_filters_partner_id').after(_select);
    for(var index in rows){

     if(rows[index].children && rows.length - 1 >  index){
       var naam = rows[index].children[columnumberaanvrager].children[0].innerHTML
       for(var index2 in docentencount[0]){

         if(naam == docentencount[0][index2]){
           $(rows[index].children[columnumberaanvrager]).append("[" + docentencount[1][index2] + "]");
           $(rows[index].children[columnumberaanvrager]).append(getSelectLink(index2));
         }
       }
     }
   }
 }
}

/***********************************************
*****SHOPOPTION EDIT ***************************
***********************************************/
if(urlSigmenten[5] == 'shopOption') {
	if($("#res_combi_so_client_id").length > 0 ) {
		addautocomplete("#res_combi_so_client_id")
	}
	tmAppender($("textarea[name='res_combi_so[opmerkingen_medewerker]']"));  
	tmAppender($("textarea[name='res_combi_so[opmerkingen_begeleider]']"));
	tmAppender($("textarea[name='res_combi_so[res_combi_so_opmerkingen]']"));

	strethfieldname($("textarea[name='res_combi_so[opmerkingen_medewerker]']"));
	strethfieldname($("textarea[name='res_combi_so[date_note]']"));
	strethfieldname($("textarea[name='res_combi_so[opmerkingen]']"));
	strethfieldname($("textarea[name='res_combi_so[opmerkingen_begeleider]']"));
	strethfieldname($("textarea[name='res_combi_so[extra_toelichting]']")); 
	var userfield = $(".user_complete");

	var userchildren = userfield.children();
	if(userchildren.length > 0){
	  var email = userchildren[2].innerHTML;
	  var naam = userchildren[0].innerHTML;
	  var gastlestype = $("#topmiddleandright h1").html().split("'")[1];

	  var school =   $(".gray tr:nth-child(3) td:nth-child(2)").html();

	  var datum = $( "#res_combi_so_date_day" ).val()+
	  '-' +$('#res_combi_so_date_month option:selected').text() + '-' +
	  $( "#res_combi_so_date_year" ).val();

	  function formatinfo(email) {
	    var info = 'Gegevens school:\n'+
	    'Docent: '+naam+'\n'+
	    'School: '+school+'\n'+
	    'Niveau: '+$('#res_combi_so_education_id option:selected').text()+
	    ' '+$('#res_combi_so_jaargang option:selected').text()+'\n'+
	    'Datum: '+$( "#res_combi_so_date_day" ).val()+
	    '-' +$("#res_combi_so_date_month option:selected").text() + '-' +
	    $( "#res_combi_so_date_year" ).val() +'\n';

	     //if email replace newlien s
	     if(email){
	      info = info.replace(/(?:\r\n|\r|\n)/g,'%0A');
	    }

	    return info;
	  }
	  
	  var datum = $( "#res_combi_so_date_day" ).val()+
	  '-' +$('#res_combi_so_date_month option:selected').text() + '-' +
	  $( "#res_combi_so_date_year" ).val();

	  var mailtolink = formatEmail(email, gastlestype, naam, datum,  school);


	  var rb = $('#res_combi_so_voorkeur');
	  console.log(rb);
	  if(rb.length){
	    var test = rb.val();
	    var rbinfo = RechtbankInfo(rb.val());

	    var datumbezoek = $( "#res_combi_so_extra_datum_day" ).val()+
	    '-' +$('#res_combi_so_extra_datum_month option:selected').text() + '-' +
	    $( "#res_combi_so_extra_datum_year" ).val();

	    var mailbody = 'Ik wil graag weer een Rechtbankbezoek voor Rechtbank' + rb.val().substr(9) + 
	    ' aanvragen. %0A%0A ' + 
	    formatinfo(true) +

	    '%0ADatum rechtbankbezoek: ' + datumbezoek + 
	    '%0AIk hoor graag wat mogelijk is.';

	    var mailtolinkRB = formatEmail(rbinfo.email, 'Rechtbankbezoek', rbinfo.aanhef, datumbezoek,  school, mailbody);

	    $('.res_combi_so_voorkeur > td').append(mailtolinkRB);
	  }
	  
	  //copyToClipboard(text);
	  userfield.first().append(mailtolink);
	  userfield.first().append('<span id="copylink">copy</span>');
	  userfield.first().append('<span id="copylink2">copy compleet</span>');
	  $("#mailtolink").click(function() {
	    //copyToClipboard("Beste "+naam+",\n\n");
	  });

	  $("#copylink").click(function() {
	    copyToClipboard(naam+', '+school);
	  });
	  
	  $("#copylink2").click(function() {
	    copyToClipboard(formatinfo());
	  });
	  document.title = school;
	  
	}

}

function getSelectLink(val){
	 var _link = $('<a>').html('selecteer ')
     _link.click(function(){
	     _select.val(val);
	     _select.trigger( "change" );
	     _select.trigger("chosen:updated");	
     });
     return _link;
}

//
function setTableSorter(selector){

	window.setTimeout(function(){
		$(selector).tablesorter()
	}, 1000);    	
}

function addautocomplete(selector){
	
	//give the injected jquery some time
	window.setTimeout(function(){
		var mySelect = $(selector);
		mySelect.chosen();		
		mySelect.before($('<a>').html('Leeg').click(function(){
			var first = $(selector+' option:first').val();
			console.log(first);
			if(first == -1){
				mySelect.val(-1);
			}
			else {
				mySelect.val(undefined);
			}
			mySelect.trigger("chosen:updated");			
			mySelect.change();			
		}));
	},
	1000);
}

function tmAppender(textfield){
  console.log(textfield);
  if(textfield.length > 0){
    textfield.change(function(event){
      var target = event.currentTarget;
      var areaEnd = target.value.substr(-myInitials.length, myInitials.length);
      if(areaEnd.toLowerCase() == myInitials.toLowerCase()){
        target.value = target.value.substr(0,target.value.length-2);
        target.value = target.value+ "("+ myInitials.toLowerCase() + ' ' + getToday() +")";
      }
    });
  }
}    



function strethfieldname(fields){
  if(fields.length > 0 ){
    fields.css("width", 500);
    fields.css("height", 100);
  }
}




function formatEmail(email,gastlestype, naam, datum, school, extrainfo){
    var url = window.location.pathname
    var path = url.split('/')
    var dossier = encodeURI("https://reserveren.prodemos.nl/prodemos/shopoption/dossier/"+path[4]);
    var extrainfo = (extrainfo) ? extrainfo : "";
    
    var html = '<div id="mailtolink"><a href="mailto:'+email+
    '?Subject='+escape(gastlestype)+ '%20'+ datum + 
    '%20'+escape(school) +
    '&body=Beste%20'+escape(naam)+',%0A'+
    '%0A '+ extrainfo +
    '%0A '+
    '%0AMet vriendelijke groet,'+
    '%0A'+
    '%0A'+
     myName +'%0A'+
    'Medewerker%20projectorganisatie%0A'+
    '070%20-%207570%20277%0A'+
    '%0A'+
    'ProDemos%20-%20Huis%20voor%20democratie%20en%20rechtsstaat'+
    '" target="_top">'+email+'</a></div>';
    return html;
}




function RechtbankInfo(rbString) {
  var rechtbanken = {
    "Rechtbank Den Haag":{
      email:"voorlichting.rb.den.haag@rechtspraak.nl",
      aanhef:"Saskia, Janne en Arnoud"
    },
    "Rechtbank Arnhem":{
      email:"groepsbezoeken.rb-gel@rechtspraak.nl",
      aanhef:"Eddy"
    },
    "Rechtbank Den Bosch":{
      email:"cib@rechtspraak.nl",
      aanhef:"mevrouw Leenders en mevrouw Ernes"    
    }
  }

  if(rechtbanken[rbString])
    return rechtbanken[rbString];
  else
    return {email:"none", aanhef:"nonde"};
}

function copyToClipboard(text) {
  window.prompt("Copy to clipboard: Ctrl+C, Enter", text);
}

function countVal(arr) {
  var a = [], b = [], prev;

  arr.sort();
  for ( var i = 0; i < arr.length; i++ ) {
    if ( arr[i] !== prev ) {
      a.push(arr[i]);
      b.push(1);
    } else {
      b[b.length-1]++;
    }
    prev = arr[i];
  }

  return [a, b];
}

function getToday(){
  var today = new Date();
  var day = today.getDate();
  var mon = month[today.getMonth()]; //January is 0!

  if(day<10) {
    day='0'+day;
  } 

  return day+mon;
}

function getDate(string, aanvraagdatum) {
  var datesplit =  string.split(' ');
  var monthNum;
  var day = datesplit[1];
  for (var i = month.length - 1; i >= 0; i--) {
    if(month[i] == datesplit[2].substr(0,3))
      monthNum = i + 1;
  };
  if(monthNum < 10)
    monthNum = '0'+ monthNum;
  
  if(day < 10)
    day = '0'+day;

  if(aanvraagdatum){
    return monthNum+'-'+day + ' '  + datesplit[0].substr(1);// + ' '  + datesplit[3].substr(0,5) ;
  }
  
  return datesplit[3]+'-'+monthNum+'-'+day;//+' '+datesplit[0];
}


})();